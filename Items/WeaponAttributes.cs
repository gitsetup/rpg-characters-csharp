﻿namespace rpg_characters
{
    public class WeaponAttributes
    {
        public int Damage { get; set; }
        public double AttackPerSecond { get; set; }
        public WeaponAttributes(int damage, double attackPerSocond)
        {
            Damage = damage;
            AttackPerSecond = attackPerSocond;
        }

        public WeaponAttributes()
        {
        }
    }
}