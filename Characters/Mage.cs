﻿

using rpg_characters.Attribute;
using rpg_characters.Items;
using rpg_characters.Types;

namespace rpg_characters.Characters
{
    public class Mage : Character
    {
        public Mage()
        {
            Type = CharacterType.Mage;
            BasePrimaryAttribute = BasePrimaryAttributes.FetchBasePrimaryAttribute(Type);

            //granted types of armor to specific charachter 
            GrantedArmorTypes.Add(ArmorType.Cloth);

            //granted types of Weapon to specific charachter 
            GrantedWeaponTypes.AddRange(new List<WeaponType> {WeaponType.Staffs, WeaponType.Wands });
        }

        /// <summary>
        /// calculates damage
        /// If equipped weapon is not null, and there is a weapon it counts out the damge
        /// 
        /// </summary>
        /// <returns></returns>
        public override double CalculateDamage()
        {
            Weapons weapons = (Weapons)Equipment[SlotType.Weapon];
            double geDamagePerSonond = weapons?.DPS ?? BaseDamage;
    
            return geDamagePerSonond * (1 + (double)TotalPrimaryAttribute.Intelligence / 100);
              
        }
    }
}
