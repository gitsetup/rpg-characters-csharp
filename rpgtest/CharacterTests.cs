﻿using rpg_characters.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpgtest
{
    public class CharacterTests
    {
        [Fact]
        public void TestCreateCharacterCheckWarriorBase_WillReturnExpectedCheckWarriorBase()
        {
            //ARRANGE
            Warrior warrior = new Warrior();
            double expectedLevel = 1;

            //ACT
            double actualLevel = warrior.Level;

            //ASSERT
            Assert.True(expectedLevel == actualLevel);

        }

        [Fact]
        public void TestCreateCharacterCheckMageBase_WillReturnExpectedCheckMageBase()
        {
            //ARRANGE
            Mage mage = new Mage();
            double expectedLevel = 1;

            //ACT
            double actualLevel = mage.Level;

            //ASSERT
            Assert.True(expectedLevel == actualLevel);

        }

        [Fact]
        public void TestCreateCharacterCheckRangerBase_WillReturnExpectedCheckRangerBase()
        {
            //ARRANGE
            Ranger ranger = new Ranger();
            double expectedLevel = 1;

            //ACT
            double actualLevel = ranger.Level;

            //ASSERT
            Assert.True(expectedLevel == actualLevel);

        }

        [Fact]
        public void TestCreateCharacterCheckRogueBase_WillReturnExpectedCheckRogueBase()
        {
            //ARRANGE
            Rogue rogue = new Rogue();
            double expectedLevel = 1;

            //ACT
            double actualLevel = rogue.Level;

            //ASSERT
            Assert.True(expectedLevel == actualLevel);

        }



        [Fact]
        public void TestLevelUpAttributeForMage_ShouldReturnLevelUpAttributesForMage()
        {
            //ARRANGE
            Mage mage = new Mage();
            mage.GetLevelUp();
            double expectedLevel = 2;

            //ACT
            double actualLevel = mage.Level;

            //ASSERT
            Assert.True(expectedLevel == actualLevel);
        }

        [Fact]
        public void TestLevelUpAttributeForWarrior_ShouldReturnLevelUpAttributesForWarrior()
        {

            //ARRANGE
            Warrior warrior = new Warrior();
            warrior.GetLevelUp();
            double expectedLevel = 2;

            //ACT
            double actualLevel = warrior.Level;

            //ASSERT
            Assert.True(expectedLevel == actualLevel);

        }

        [Fact]
        public void TestLevelUpAttributeForRogue_ShouldReturnLevelUpAttributesForRouge()
        {

            //ARRANGE
            Rogue rogue = new Rogue();
            rogue.GetLevelUp();
            double expectedLevel = 2;

            //ACT
            double actualLevel = rogue.Level;

            //ASSERT
            Assert.True(expectedLevel == actualLevel);
        }
    }
}
