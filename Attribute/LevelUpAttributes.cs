﻿using rpg_characters.Exceptions;
using rpg_characters.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters.Attribute
{
    /// <summary>
    /// Method for Level up, based on Character Type, they will assign the values
    /// Also a invalid Character Exception
    /// </summary>
    public class LevelUpAttributes
    {
        public static PrimaryAttribute LevelUpAttribute(CharacterType characterType)
        {
            switch (characterType)
            {
                case CharacterType.Mage:
                    return new PrimaryAttribute() { Strength = 1, Dexterity = 1, Intelligence = 5 };
                    break;
                case CharacterType.Rogue:
                    return new PrimaryAttribute() { Strength = 1, Dexterity = 4, Intelligence = 1 };
                    break;
                case CharacterType.Warrior:
                    return new PrimaryAttribute() { Strength = 3, Dexterity = 2, Intelligence = 1 };
                    break;
                case CharacterType.Ranger:
                    return new PrimaryAttribute() { Strength = 1, Dexterity = 5, Intelligence = 1 };
                    break;
                default: throw new ÍnvalidCharacterException($"Invalid Character type, {characterType}");
            }

        }
    }
}
