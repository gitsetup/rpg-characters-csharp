﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters.Types
{
    public enum SlotType
    {
        Head,
        Body,
        Legs,
        Weapon
    }
}
