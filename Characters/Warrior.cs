﻿using rpg_characters.Attribute;
using rpg_characters.Items;
using rpg_characters.Types;

namespace rpg_characters.Characters
{
    public class Warrior : Character
    {
        public Warrior()
        {
            Type = CharacterType.Warrior;
            BasePrimaryAttribute = BasePrimaryAttributes.FetchBasePrimaryAttribute(Type);
         
            //granted types of armor to specific charachter 
            GrantedArmorTypes.AddRange(new List<ArmorType> { ArmorType.Plate, ArmorType.Mail});

            //granted types of Weapon to specific charachter 
            GrantedWeaponTypes.AddRange(new List<WeaponType> { WeaponType.Axes, WeaponType.Swords, WeaponType.Hammers });
        }

        public Warrior(string name)
        {
            Name = name;
        }

        /// <summary>
        /// calculates damage
        /// If equipped weapon is not null, and there is a weapon it counts out the damge
        /// return the total damage
        /// </summary>
        /// <returns></returns>
        public override double CalculateDamage()
        {
            Weapons? equippedWeapon = (Weapons?)Equipment[SlotType.Weapon];
            double damagePerSecond = equippedWeapon?.DPS ?? BaseDamage;

            return damagePerSecond * (1 + (double)TotalPrimaryAttribute.Strength / 100);

        }
    }
}