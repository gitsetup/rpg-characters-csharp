﻿
using rpg_characters.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters.Items
{
    public class Item
    {
        public string Name { get; set; } = "";
        public int LevelRequired { get; set; } = 1;
        public SlotType Slot { get; set; }
    }
}
