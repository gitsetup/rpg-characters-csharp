﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters.Exceptions
{
    public class InvalidCharacterLevelException:Exception
    {

        public InvalidCharacterLevelException(string message) : base(message)
        {

        }

        public InvalidCharacterLevelException(string message, Exception inner) : base(message, inner)
        {

        }

    }
}
