﻿using rpg_characters.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters.Items
{
    public class Weapons: Item
    {
        public WeaponType WeaponType { get; set; }
        public WeaponAttributes WeaponsAttribute { get; set; }
        //Damage Per Second
        public double DPS { get => WeaponsAttribute.Damage * WeaponsAttribute.AttackPerSecond; }

        public Weapons()
        {
        }
        public Weapons( WeaponType weaponType, int levelRequired, string name, SlotType slot, WeaponAttributes weaponAttributes)
        {
            WeaponType = weaponType;
            Name = name;
            LevelRequired = levelRequired;
            Slot = slot;
            WeaponsAttribute = weaponAttributes;
        }
    }
}   
