﻿

using rpg_characters.Attribute;
using rpg_characters.Exceptions;
using rpg_characters.Items;
using rpg_characters.Types;
using System.Text;

namespace rpg_characters.Characters
{
    public abstract class Character
    {
        public string Name { get; set; } = "";
        public int Level { get; set; } = 1;
        public CharacterType Type { get; set; }
        public PrimaryAttribute BasePrimaryAttribute { get; set; } = new PrimaryAttribute();

        //Calculate total 
        public PrimaryAttribute TotalPrimaryAttribute => CalculateTotalAttribute();

        public const double BaseDamage = 1.0;

        //GrantedArmor
        public List<ArmorType> GrantedArmorTypes = new List<ArmorType>();

        //GrantedWeapon
        public List<WeaponType> GrantedWeaponTypes = new List<WeaponType>();

        //Slot Dictonary
        public Dictionary<SlotType, Item> Equipment { get; } = new Dictionary<SlotType, Item>() {

            {SlotType.Weapon,null },
            {SlotType.Body, null },
            {SlotType.Head, null },
            {SlotType.Legs, null },

        };
        
        

        /// <summary>
        /// METHOD FOR level up the attributes
        /// starting with defualt level
        /// and adding to primary
        /// </summary>
        /// <param name="levelDefault"></param>
        public virtual void GetLevelUp()
        {
            int levelDefault = 1;

            if (levelDefault < 1)
            {
                Console.WriteLine("Heros level is lower than expected");
            }
            Level++;

            BasePrimaryAttribute += LevelUpAttributes.LevelUpAttribute(Type) * levelDefault;
        }

        /// <summary>
        /// Calculate total attribute
        /// looping through slot in Equipment
        /// if slot value != bnull && != SlotType Weapon
        /// Adding attr with help of Aggregate
        /// returning base and the equippedArmorValues
        /// </summary>
        /// 
        private PrimaryAttribute CalculateTotalAttribute()
       {
        
            PrimaryAttribute equippedArmorAttributes = new PrimaryAttribute();
            IEnumerable<Armor> enumerable()
            {
                foreach (var slot in Equipment)
                {
                    if (slot.Value != null && slot.Key != SlotType.Weapon)
                    {
                        yield return (Armor)slot.Value;
                    }
                }
            }
            equippedArmorAttributes = enumerable().Aggregate(equippedArmorAttributes, (equip, equipSlot) => equip + equipSlot.PrimaryAttribute);

            return BasePrimaryAttribute + equippedArmorAttributes;
        }



        /// <summary>
        /// Method for equip the character with two "controls"
        /// if right armortype and level
        /// and 
        /// if meeting level requirments
        /// </summary>
        /// <param name="armor"></param>
        /// <returns></returns>
        /// <exception cref="InvalidArmorException"></exception>
public virtual string CharacterEquipArmor(Armor armor)
        {
            //GrantedArmorTypes

            if (!GrantedArmorTypes.Contains(armor.ArmorType))
            {
                throw new InvalidArmorException($"{armor.ArmorType}, is not compatable with character type");
            }
            if(armor.LevelRequired > Level) {

                throw new InvalidArmorException($"Armor type level {armor.LevelRequired} is higher then level you are ");
            }

            Equipment[armor.Slot] = armor;
            return $"The new armor equimpent"; 

        }

        /// <summary>
        /// Method for equip the character with two "controlls" 
        /// one for right type of weapon and level 
        /// and 
        /// right level 
        /// </summary>
        /// <param name="weapons"></param>
        /// <returns></returns>
        /// <exception cref="InvalidWeaponException"></exception>

        public virtual string CharacterEquipWeapon(Weapons weapons)
        {
            if (!GrantedWeaponTypes.Contains(weapons.WeaponType))
            {
                throw new InvalidWeaponException($"Invalid {weapons.WeaponType} for the character");
            }
            if (weapons.LevelRequired > Level)
            {
                throw new InvalidWeaponException($"Weapon type level {weapons.LevelRequired} is too high for your character");
            }

            Equipment[weapons.Slot] = weapons;
            return $"The new weapon equipemnt";

        }

        public abstract double CalculateDamage();


        /// <summary>
        /// Displays Character stats with stringBuilder
        /// </summary>
        public virtual void DisplayCharacterStats()
        {
                StringBuilder dispayCharacterStats = new StringBuilder();
                dispayCharacterStats.AppendLine("Characters name: " + Name);
                dispayCharacterStats.AppendLine("Characters Level " + Level);
                dispayCharacterStats.AppendLine("Characters Strength " + TotalPrimaryAttribute.Strength);
                dispayCharacterStats.AppendLine("Characters Dexterity " + TotalPrimaryAttribute.Dexterity);
                dispayCharacterStats.AppendLine("Characters Intelligence " + TotalPrimaryAttribute.Intelligence);
                dispayCharacterStats.AppendLine("Characters Damage " + CalculateDamage());
                Console.WriteLine(dispayCharacterStats.ToString());
            
        }
    }
}
