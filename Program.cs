﻿using rpg_characters;
using rpg_characters.Attribute;
using rpg_characters.Characters;
using rpg_characters.Items;
using rpg_characters.Types;

Mage mage = new Mage();

mage.Name = "Elmin";

Console.WriteLine(mage.Name);
mage.GetLevelUp();
mage.GetLevelUp();
Console.WriteLine("Current level: " + mage.Level);

Console.WriteLine("Current intell " + mage.TotalPrimaryAttribute.Intelligence);

Weapons weaponToAdd = new Weapons()
{
    LevelRequired = 1,
    Name = "Staffye",
    WeaponType = WeaponType.Staffs,
    WeaponsAttribute = new WeaponAttributes() { Damage=1, AttackPerSecond= 1.2},
    Slot = SlotType.Weapon

};

mage.CharacterEquipWeapon(weaponToAdd);

Armor Cloth = new Armor()
{
    ArmorType = ArmorType.Cloth,
    LevelRequired = 1,
    Name = "Clothie",
    PrimaryAttribute = new PrimaryAttribute() { Intelligence = 5 },
    Slot = SlotType.Body
};

mage.CharacterEquipArmor(Cloth);


mage.DisplayCharacterStats();
mage.GetLevelUp();

Armor newCloth = new Armor()
{
    ArmorType = ArmorType.Cloth,
    LevelRequired = 4,
    Name = "Clothie",
    PrimaryAttribute = new PrimaryAttribute() { Intelligence = 5 },
    Slot = SlotType.Body
};
mage.CharacterEquipArmor(newCloth);

mage.DisplayCharacterStats();
