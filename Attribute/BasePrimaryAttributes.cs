﻿

using rpg_characters.Exceptions;
using rpg_characters.Types;

namespace rpg_characters.Attribute
{
    /// <summary>
    /// method for fetching the base attributes for the charcters 
    /// with swich based on character Type
    /// </summary>
    public class BasePrimaryAttributes: PrimaryAttribute
    {
        public static PrimaryAttribute FetchBasePrimaryAttribute(CharacterType characterType)
        {
            switch (characterType)
            {
                case CharacterType.Mage:
                    return new PrimaryAttribute() { Strength = 1, Dexterity = 1, Intelligence = 8 };
                    break;
                case CharacterType.Rogue:
                    return new PrimaryAttribute() { Strength = 2,Dexterity = 6,Intelligence = 1};
                    break;
                case CharacterType.Warrior:
                    return new PrimaryAttribute() { Strength = 3, Dexterity = 2,Intelligence = 1};
                    break;
                case CharacterType.Ranger:
                    return new PrimaryAttribute() { Strength = 1, Dexterity = 7,Intelligence = 1};
                    break;
                default: throw new ÍnvalidCharacterException($"Invalid Character type, {characterType}");
            }
        }

    }
}


