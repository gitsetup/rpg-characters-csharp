﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters.Exceptions
{
    public class ÍnvalidCharacterException: Exception
    {
        public ÍnvalidCharacterException()
        {

        }

        public ÍnvalidCharacterException(string message) : base(message)
        {
           
        }

        public ÍnvalidCharacterException(string message, Exception inner): base(message, inner)
        {

        }

    }
}
