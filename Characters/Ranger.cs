﻿using rpg_characters.Attribute;
using rpg_characters.Items;
using rpg_characters.Types;

namespace rpg_characters.Characters
{
    public class Ranger : Character
    {
        public Ranger()
        {
            Type = CharacterType.Ranger;
            BasePrimaryAttribute = BasePrimaryAttributes.FetchBasePrimaryAttribute(Type);

            //granted types of armor to specific charachter 
            GrantedArmorTypes.AddRange(new List<ArmorType> {ArmorType.Leather, ArmorType.Mail });

            //granted types of Weapon to specific charachter 
            GrantedWeaponTypes.Add(WeaponType.Bows);
        }


        /// <summary>
        /// calculates damage
        /// If equipped weapon is not null, and there is a weapon it counts out the damge
        /// 
        /// </summary>
        /// <returns></returns>
        public override double CalculateDamage()
        {
            Weapons? equippedWeapon = (Weapons?)Equipment[SlotType.Weapon];
            double getDamagePerSecond = equippedWeapon?.DPS ?? BaseDamage;
            return getDamagePerSecond * (1 + (double)TotalPrimaryAttribute.Dexterity / 100);
        }
    }
}