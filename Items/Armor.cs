﻿using rpg_characters.Attribute;
using rpg_characters.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters.Items
{
    public class Armor: Item
    {
        public ArmorType ArmorType { get; set; }

        public PrimaryAttribute PrimaryAttribute { get; set; }

        public Armor()
        {
        }

        public Armor(ArmorType armorType, PrimaryAttribute primaryAttribute, string name, int levelRequired, SlotType slot )
        {
            ArmorType = armorType;
            PrimaryAttribute = primaryAttribute;
            Name = name;
            LevelRequired = levelRequired;
            Slot = slot;
        }

       
    }
}
