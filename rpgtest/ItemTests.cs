﻿using rpg_characters;
using rpg_characters.Attribute;
using rpg_characters.Characters;
using rpg_characters.Exceptions;
using rpg_characters.Items;
using rpg_characters.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpgtest
{
    public class ItemTests
    {

        Weapons testAxe = new Weapons()
        {
            Name = "Common axe",
            LevelRequired = 1,
            Slot = SlotType.Weapon,
            WeaponType = WeaponType.Axes,
            WeaponsAttribute = new WeaponAttributes(damage: 7, attackPerSocond: 1.1)
        };

        Armor testPlateBody = new Armor()
        {
            Name = "Common plate body armor",
            LevelRequired = 1,
            Slot = SlotType.Body,
            ArmorType = ArmorType.Plate,
            PrimaryAttribute = new PrimaryAttribute() { Strength = 1, Dexterity = 0, Intelligence = 0 }
        };

        Weapons testBow = new Weapons()
        {
            Name = "Common bow",
            LevelRequired = 1,
            Slot = SlotType.Weapon,
            WeaponType = WeaponType.Bows,
            WeaponsAttribute = new WeaponAttributes() { Damage = 12, AttackPerSecond = 0.8 }
        };

        [Fact]
        public void TestLevelUpAttributeForRanger_ShouldReturnLevelUpAttributesForRanger()
        {
            //ARRANGE
            Ranger ranger = new Ranger();
            ranger.GetLevelUp();
            double expectedLevel = 2;

            //ACT
            double actualLevel = ranger.Level;

            //ASSERT
            Assert.True(expectedLevel == actualLevel);
        }

        [Fact]
        public void TestWeaponTestStaffs_ShouldReturnEqualWeaponTestStaffs()
        {
            // Arrange
            Weapons testStaff = new Weapons()
            {
                Name = "Staffie",
                LevelRequired = 1,
                Slot = SlotType.Weapon,
                WeaponType = WeaponType.Staffs,
                WeaponsAttribute = new WeaponAttributes() { Damage = 2, AttackPerSecond = 1.1 }
            };
            double expected = 2 * 1.1;
            // Act
            double actual = testStaff.DPS;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestWeaponTestSword_ShouldReturnEqualWeaponTestSword()
        {
            //Arrange
            Weapons testSword = new Weapons()
            {
                Name = "Swordie",
                LevelRequired = 1,
                Slot = SlotType.Weapon,
                WeaponType = WeaponType.Swords,
                WeaponsAttribute = new WeaponAttributes() { Damage = 2, AttackPerSecond = 2.2 }
            };
            double expected = 2 * 2.2;

            //Act
            double actual = testSword.DPS;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestIfCharacterEquipHigherLevelWeaponSholdReturnInvalidWeaponExeption()
        {
            // Arrange
            Weapons testSword = new Weapons()
            {
                Name = "Staffie",
                LevelRequired = 2,
                Slot = SlotType.Weapon,
                WeaponType = WeaponType.Swords,
                WeaponsAttribute = new WeaponAttributes() { Damage = 2, AttackPerSecond = 1.1 }
            };

            Warrior warrior = new Warrior()
            {
                Name = "Elmin",
                BasePrimaryAttribute = new PrimaryAttribute(1, 5, 2),
                GrantedArmorTypes = new List<ArmorType>(new List<ArmorType> { ArmorType.Plate, ArmorType.Mail }),
                GrantedWeaponTypes = new List<WeaponType> { WeaponType.Axes, WeaponType.Swords, WeaponType.Hammers },
                Level = 1,
                Type = CharacterType.Warrior
            };

            int actual = testSword.LevelRequired;
            Assert.Throws<InvalidWeaponException>(() => warrior.CharacterEquipWeapon(testSword));
        }

        [Fact]
        public void TestInvalidArmorException_ShouldReturnInvalidArmorException()
        {
            //Arrange
            Armor armor = new Armor()
            {
                ArmorType = ArmorType.Plate,
                LevelRequired = 2,
                PrimaryAttribute = new PrimaryAttribute(1, 5, 2),
                Slot = SlotType.Body
            };


            Warrior warrior = new Warrior()
            {
                Name = "Elmin",
                BasePrimaryAttribute = new PrimaryAttribute(1, 5, 2),
                GrantedArmorTypes = new List<ArmorType>(new List<ArmorType> { ArmorType.Plate, ArmorType.Mail }),
                GrantedWeaponTypes = new List<WeaponType> { WeaponType.Axes, WeaponType.Swords, WeaponType.Hammers },
                Level = 1,
                Type = CharacterType.Warrior
            };
            Assert.Throws<InvalidArmorException>(() => warrior.CharacterEquipArmor(armor));

        }

        [Fact]

        public void TestInvalidArmorType_ShouldReturnInvalidArmorException()
        {
            //Arrange
            Armor armor = new Armor()
            {
                ArmorType = ArmorType.Cloth,
                LevelRequired = 1,
                PrimaryAttribute = new PrimaryAttribute(1, 5, 2),
                Slot = SlotType.Body
            };

            Warrior warrior = new Warrior()
            {
                Name = "Elmin",
                BasePrimaryAttribute = new PrimaryAttribute(1, 5, 2),
                GrantedArmorTypes = new List<ArmorType>(new List<ArmorType> { ArmorType.Plate, ArmorType.Mail }),
                GrantedWeaponTypes = new List<WeaponType> { WeaponType.Axes, WeaponType.Swords, WeaponType.Hammers },
                Level = 1,
                Type = CharacterType.Warrior
            };

            Assert.Throws<InvalidArmorException>(() => warrior.CharacterEquipArmor(armor));

        }

        [Fact]
        public void TestInvalidWeaponType_ExpectedBehaviourToReturnwarriorInvalidWeaponType()
        {
            //Arrange
            Warrior warrior = new Warrior();

            Weapons testBow = new Weapons()
            {
                Name = "Common bow",
                LevelRequired = 1,
                Slot = SlotType.Weapon,
                WeaponType = WeaponType.Bows,
                WeaponsAttribute = new WeaponAttributes() { Damage = 12, AttackPerSecond = 0.8 }
            };

            //Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.CharacterEquipWeapon(testBow));

        }


        [Fact]
        public void TestValidWeaponInEquipment_ShouldReturnNewWeaponEquipment()
        {
            //Arrange
            Warrior warrior = new Warrior()
            {
                Name = "Elmin",
                BasePrimaryAttribute = new PrimaryAttribute(1, 5, 2),
                GrantedArmorTypes = new List<ArmorType>(new List<ArmorType> { ArmorType.Plate, ArmorType.Mail }),
                GrantedWeaponTypes = new List<WeaponType> { WeaponType.Axes, WeaponType.Swords, WeaponType.Hammers },
                Level = 1,
                Type = CharacterType.Warrior
            };

            Weapons TestSword = new Weapons()
            {
                Name = "Sword",
                LevelRequired = 1,
                Slot = SlotType.Weapon,
                WeaponType = WeaponType.Swords,
                WeaponsAttribute = new WeaponAttributes() { Damage = 2, AttackPerSecond = 1.1 }
            };

            Weapons expected = TestSword;

            // Act
            warrior.CharacterEquipWeapon(TestSword);
            Item? actual = warrior.Equipment[SlotType.Weapon];

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]

        public void TestValidArmor_ExpectedBehaviourToReturnValidArmorEquipt()
        {
            //Arrange
            Warrior warrior = new Warrior();
            string expected = "The new armor equimpent";

            //Act
            string actual = warrior.CharacterEquipArmor(testPlateBody);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestValidWeapon_ExpectedBehaviourToReturnValidWeaponEquipt()
        {
            //Arrange
            Warrior warrior = new Warrior();
            string expected = "The new weapon equipemnt";

            //Act 
            string actual = warrior.CharacterEquipWeapon(testAxe);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void
        TestCalculateDamageWithOutWeapon_ExpectedBehaviourToReturnCalculateDamageWithOutWeapon()
        {
            //Arrange
            Warrior warrior = new Warrior();
            int warriorBase = BasePrimaryAttributes.FetchBasePrimaryAttribute(CharacterType.Warrior).Strength;
            double expected = Character.BaseDamage * (1 + (double)warriorBase / 100);

            //Act
            double actual = warrior.CalculateDamage();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void
        TestCalculateDamageWithValidWeaponEquipped_ExpectedBehaviourToReturnValidWeaponEquipped()
        {
            //Arrange
            Warrior warrior = new Warrior();

            int warriorBase = BasePrimaryAttributes.FetchBasePrimaryAttribute(CharacterType.Warrior).Strength;
            double weaponDamagePerSecond = testAxe.DPS;
            double expected = weaponDamagePerSecond * (1 + (double)warriorBase / 100);

            //Act
            warrior.CharacterEquipWeapon(testAxe);
            double actual = warrior.CalculateDamage();

            //Assert
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void
        TestCalculateDamageWithValidWeaponAndArmorEquipped_ExpectedBehaviourToReturnValidWeaponAndArmorEquipped()
        {
            //Arrange
            Warrior warrior = new Warrior();
            int warriorBase = BasePrimaryAttributes.FetchBasePrimaryAttribute(CharacterType.Warrior).Strength;
            int totalPrimaryAttribute = warriorBase + testPlateBody.PrimaryAttribute.Strength;
            double weaponDamagePerSecond = testAxe.DPS;
            double expected = weaponDamagePerSecond * (1 + (double)totalPrimaryAttribute / 100);

            //Act
            warrior.CharacterEquipWeapon(testAxe);
            warrior.CharacterEquipArmor(testPlateBody);
            double actual = warrior.CalculateDamage();

            //Assert
            Assert.Equal(expected, actual);

        }

    }
}
