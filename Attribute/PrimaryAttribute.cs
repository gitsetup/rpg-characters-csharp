﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters.Attribute
{
    public class PrimaryAttribute
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        #region PrimaryAttribute
        public PrimaryAttribute(int intelligence = 0, int strength = 0, int dexterity = 0 )
        {
            Intelligence = intelligence;
            Strength = strength;
            Dexterity = dexterity;
        }

        public static PrimaryAttribute operator +(PrimaryAttribute lhs, PrimaryAttribute rhs)
        {
            return new PrimaryAttribute
            {
                Strength = lhs.Strength + rhs.Strength,
                Dexterity = lhs.Dexterity + rhs.Dexterity,
                Intelligence = lhs.Intelligence + rhs.Intelligence
            };
        }

        public static PrimaryAttribute operator *(PrimaryAttribute lhs, int rhs)
        {
            return new PrimaryAttribute
            {
                Strength = lhs.Strength * rhs,
                Dexterity = lhs.Dexterity * rhs,
                Intelligence = lhs.Intelligence * rhs
            };
        }

        
        #endregion

    }


  

}
