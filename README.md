# Assigment 1 
Assigment 1. RPG Characters 

## Install

```
git clone https://gitlab.com/gitsetup/rpg-characters-csharp.git

```
## Usage 

# Development
This application is created with C# and .NET as a console application. In the application you can equip the character with armor and weapons.
The characters can level up, wich is neccesery if you want to equip the character with higher level equipment. 
In the RPG character console app, there are four main characters Warrior, Ranger, Mage and Rogue. Each character has diffrent attributes and equipment settings. 
Important to know of the console application:
It's not completed to perform a game. Only main requirments are meet of assignment.  

## Dependencies
 - Windows 10
 - Visual Studio Community 2022(.NET 6)
 - XUnit


## Contributors

This assignment has been done by [Elmin Nurkic](@gfunkmaster) 

## Acknowledgments
 - Assigment for a course in C# and .NET. Course provided by Noroff Education AS.


